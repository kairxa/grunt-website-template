(function() {
	'use strict';

	module.exports = function(grunt) {
		grunt.initConfig({
			pkg: grunt.file.readJSON('package.json'),
			sass: {
				dev: {
					options: {
						style: 'expanded'
					},
					files: {
						'assets/stylesheet/all.css' : 'assets/stylesheet/scss/style.scss'
					}
				},
                prod: {
                    options: {
                        style: 'compressed'
                    },
                    files: {
                        'assets/stylesheet/all.min.css' : 'assets/stylesheet/all.css'
                    }
                }
			},
			autoprefixer: {
				dist: {
					files: {
						'assets/stylesheet/all.css' : 'assets/stylesheet/all.css'
					}
				}
			},
			jshint: {
				files: [
					'Gruntfile.js',
					'assets/javascript/partials/*.js'
				],
				options: {
					reporter: require('jshint-stylish'),
					jshintrc: true
				}
			},
			concat: {
				dist: {
					src: [
						'bower_components/jquery/dist/jquery.js',

						// LOAD BOOTSTRAP JS MODULES
						// LOAD ONLY THINGS YOU NEEDED
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/affix.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/alert.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/button.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/carousel.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/popover.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/tab.js',
						// 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',

						// USER CRAFTED JS FILES
						'assets/javascript/partials/*.js'
					],
					dest: 'assets/javascript/all.js'
				}
			},
			uglify: {
				build: {
					src: 'assets/javascript/all.js',
					dest: 'assets/javascript/all.min.js'
				}
			},
			watch: {
				options: {
					nospawn: true,
					livereload: true
				},
				js: {
					files: ['assets/javascript/partials/*.js', 'Gruntfile.js'],
					tasks: ['concat', 'jshint', 'uglify']
				},
				css: {
					files: 'assets/stylesheet/scss/**/*.scss',
					tasks: ['sass', 'autoprefixer']
				}
			},
			copy: {
				bootstrapcss: {
					files: [
						{
							expand: true,
							cwd: 'bower_components/bootstrap-sass/assets/stylesheets/bootstrap/',
							src: '**',
							dest: 'assets/stylesheet/scss/bootstrap/',
							flatten: false
						},
						{
							expand: true,
							cwd: 'bower_components/bootstrap-sass/assets/stylesheets/',
							src: '_bootstrap.scss',
							dest: 'assets/stylesheet/scss/',
							filter: 'isFile',
							rename: function (dest, src) {
								return dest + src.replace(/_bootstrap/, '_bootstrap-custom');
							}
						}
					]
				}
			}
		});

		grunt.loadNpmTasks('grunt-contrib-sass');
		grunt.loadNpmTasks('grunt-autoprefixer');
		grunt.loadNpmTasks('grunt-contrib-jshint');
		grunt.loadNpmTasks('grunt-contrib-concat');
		grunt.loadNpmTasks('grunt-contrib-uglify');
		grunt.loadNpmTasks('grunt-contrib-watch');
		grunt.loadNpmTasks('grunt-contrib-copy');

		grunt.registerTask('default', ['sass:dev', 'autoprefixer', 'jshint', 'concat', 'watch']);
		grunt.registerTask('copybscss', ['copy:bootstrapcss']);
        grunt.registerTask('shrinkify', ['uglify', 'sass:prod']);
	};
})();